<?php
require "functions.php";
require_once "page/header.php"; //Header
//Check if people can register before continue
if (fetchval('wiki_enablereg') == 0){die("<h2>Registering is disabled.</h2>");}

if (!isset($_POST['email']) || !isset($_POST['nick']) || !isset($_POST['passver'])){
?>
<div style="text-align: center;position: absolute;left: 50%;margin-left: -280px;margin-top:20px;">
<form id="form1" method="post">
<table>
	<tr>
		<td class="textT">Nick:(20 car. max)</td>
		<td><input name="nick" type="text" placeholder="Tu nick" maxlength="20" required=”required”></td>
	</tr>
	<tr>
		<td class="textT">Email:</td>
		<td><input name="email" type="text" placeholder="E-mail" required=”required”></td>
	</tr>
	<tr>
		<td class="textT">Password:</td>
		<td><input name="pass" type="password" placeholder="Contraseña" required=”required”></td>
	</tr>
	<tr>
		<td class="textT">Repeat password:</td>
		<td><input name="passver" type="password" placeholder="Por seguridad" required=”required”></td>
	</tr>
	<tr>
		<td class="textT">Verify: Write "robot" backwards</td>
		<td><input name="verif" type="text" placeholder="No queremos robots en esta web" required=”required”></td>
	</tr>
	<tr>
		<td colspan="2">If you press register, you will automatically accept the <a href="tos.php">Terms Of Service</a></td>
	</tr>
	<tr>
		<td><button type="reset" class="btnred">Erase fields</button></td>
		<td><button type="submit" class="btn">Register</button></td>
	</tr>
</table>
</form>
</div>
<?php
	
}else{
	$nick=$_POST['nick'];
	$passw=hash("sha256",$_POST['pass']);
	$email=$_POST['email'];

	if ($_POST['pass'] != $_POST['passver']){
	echo "<h2>Contraseña mal escrita, volviendo..</h2>";
		?>
		<script type='text/javascript'>
		alert('Contraseña mal escrita, vuelva a escribirla');
		top.location = "register.php";
	</script>
		<?php
		die();
	}

	if ($_POST['verif'] != "tobor"){
	echo "<h2>Captcha erróneo</h2>";
			?>
		<script type='text/javascript'>
		alert('Captcha erróneo');
		top.location = "register.php";
	</script>
		<?php
		die();
	}	
		$contar = dbw_query_fetch_array($db_conn,"SELECT COUNT(ID) FROM `users` WHERE nick = '$nick'")[0];

	if ($contar == "0")
	{
		$nick=$_POST['nick'];
		dbw_query($db_conn,"INSERT INTO `users`(`nick`, `passw`, `email`) VALUES ('$nick','$passw','$email')");
		echo  "<h2>Registrado</h2><a href='index.php'>Inicio</a>";
	}else
	{
		print "Este usuario ya existe, clica el link y vuelva a rellenar los datos";
		echo "<a href='register.php'>Registrar</a>";
	}	
}

require_once "page/footer.php"; //Finish webpage