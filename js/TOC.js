// Credits to http://www.wextensible.com/articulos/tabla-contenidos/ 
function creartoc() {
	/* Con estas variables podemos limitar los elementos de 
	 * encabezado, por ejemplo desde h1 hasta h6 (van de h1 a h6)
	 */
	var desdeH = 2;
	var hastaH = 6;
	var prefijoToc = "_toc";
	var toc = document.getElementById("TOCdiv");
	var todos = document.body.getElementsByTagName("*");
	var haches = "";
	var numHaches = 0;
	var numId = 0;
	for (var i=0; i<todos.length; i++){
		if (todos[i].tagName){
			var tag = todos[i].tagName.toLowerCase();
			if  ((tag.length == 2) && (tag[0] == "h")){
				var numH = parseInt(tag[1]);
				if ((!isNaN(numH)) && (numH >= desdeH)
				&&(numH <= hastaH)) {
					//Si no posee un id se lo ponemos
					if (!todos[i].id) {
						numId++;
						todos[i].setAttribute("id", prefijoToc + numId);
					}                        
					haches += '<li style="text-indent:' + numH +
					'em"><a href="#' + 
					todos[i].id + '" onclick="cerrarToc()">' + 
					todos[i].innerHTML + '</a></li>';
					numHaches++;
				}
			}
		}
	}
	if (numHaches == 0) {
		haches += '<li>No hay encabezados</li>';
	}
	haches = "<b>Tabla de contenidos</b>" +
	'<ul>' + haches +  "</ul>";
	toc.innerHTML = haches;
}
window.onload = creartoc;