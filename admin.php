<?php
require "functions.php";
requirelogin();
require_once "page/header.php"; //Header
if (1 != $user['typeuser']){die('You dont have admin rights');} //This blocks edit or create pages if mintypeuser is below the typeuser.

switch ($_GET['do']){
	case "savewikicat":
		dbw_query($db_conn, "INSERT INTO `cats` (`catname`) VALUES ('$_POST[catname]');");
		break;
	case "savewikiopt":
		dbw_query($db_conn, "UPDATE system_opts SET value='$_POST[wiki_title]' WHERE option='wiki_title'");
		dbw_query($db_conn, "UPDATE system_opts SET value='$_POST[wiki_subtitle]' WHERE option='wiki_subtitle'");
		dbw_query($db_conn, "UPDATE system_opts SET value='$_POST[editpages_mintypeuser]' WHERE option='editpages_mintypeuser'");
		dbw_query($db_conn, "UPDATE system_opts SET value='$_POST[defparse_type]' WHERE option='defparse_type'");
		dbw_query($db_conn, "UPDATE system_opts SET value='$_POST[wiki_enablereg]' WHERE option='wiki_enablereg'");
		dbw_query($db_conn, "UPDATE system_opts SET value='$_POST[wiki_aboutpage]' WHERE option='wiki_aboutpage'");
		break;
	case "savewikiheader":
		dbw_query($db_conn, "UPDATE system_opts SET value='$_POST[headlinks_loc]' WHERE option='headlinks_loc'");
		$e_text = explode(",",$_POST['head_linktxt']); //Los 4 textos de links seguidos
		$e_link = explode(",",$_POST['head_links']); //Los 4 links seguidos
		$final = $e_text[0] . "," . $e_link[0] . "," . $e_text[1] . "," . $e_link[1] . "," . $e_text[2] . "," . $e_link[2] . "," . $e_text[3] . "," . $e_link[3]; //Unir todo
		dbw_query($db_conn, "UPDATE system_opts SET value='$final' WHERE option='head_links'");
		break;
	case "savewikiside":
		dbw_query($db_conn, "UPDATE system_opts SET value='$_POST[side_text]' WHERE option='side_text'");
		dbw_query($db_conn, "UPDATE system_opts SET value='$_POST[side_pages]' WHERE option='side_pages'");
		break;
	case "savewikiappearance":
		dbw_query($db_conn, "UPDATE styles SET value='$_POST[hdr_clr]' WHERE style='hdr_clr'");
		dbw_query($db_conn, "UPDATE styles SET value='$_POST[hdr_bar]' WHERE style='hdr_bar'");
		dbw_query($db_conn, "UPDATE styles SET value='$_POST[side_clr]' WHERE style='side_clr'");
		dbw_query($db_conn, "UPDATE styles SET value='$_POST[side_inclr]' WHERE style='side_inclr'");
		break;
	case "restorewikiappearance":
		dbw_query($db_conn, "UPDATE styles SET value=''"); //Erases all styles
		break;
	case "uploadlogo":
		$target = "images/logoheader.png";
		$uploadOk = 1;
		// Check if image file is a actual image or fake image
		$check = getimagesize($_FILES["logoimage"]["tmp_name"]);
		if($check !== false) {$uploadOk = 1;} else {$uploadOk = 0;}
		if ($uploadOk == 0) {
			echo "Hubo un error.";
		} else {move_uploaded_file($_FILES["logoimage"]["tmp_name"], $target);}
		break;
	case "borrarcat": //This is called also from wiki.php in empty category
		$ids = dbw_query_fetch_array($conexion, "SELECT ID,bdtitle FROM pagerefs WHERE idcat ='$id'");
		if ($ids){
			echo "<h3>Aun hay articulos en esta categoría, no se puede eliminar</h3>";
		}else{
			dbw_query($db_conn, "DELETE FROM `cats` WHERE catname='$_GET[cat]'");
			echo "<p>Categoría eliminada</p>";
		}
		break;
}

?>
<script>
function mostrar(div) {
	obj = document.getElementById(div);
	obj.style.display = (obj.style.display == 'none') ? 'block' : 'none';
}
</script>
<?php
$skin_colors = fetchval('skin_colors');
$head_links = rtrim(fetchval('head_links'),","); //Delete "," that are at finish
$E_head_links = explode(",",$head_links); //Exploded by commas head_links
$numlinks = count($E_head_links);

for ($i = 0; $i <= $numlinks; $i = $i + 2){ //Show links if it exists
	if ($E_head_links[$i]){
		$m = $i + 1;
		$head_linktxt_form .= $E_head_links[$i] . ","; 
		$head_links_form .= $E_head_links[$m] . ","; 
	}
}
$head_linktxt_form = rtrim($head_linktxt_form,",");
$head_links_form = rtrim($head_links_form,",");
?>
<h3 class="admincat"><a href="#" onclick="mostrar('wikicat')">New category</a></h3>
<div id="wikicat" style="border-collapse:collapse;display:none;">
	<form action="admin.php?do=savewikicat" method="post">
	<table>
		<tr>
			<td>Name of the new category:</td>
			<td><input type="text" name="catname" placeholder="No usar espacios."></td>
		</tr>
	</table>
	<button type="submit" class="btn">Add category</button>
	</form>
</div>
<h3 class="admincat"><a href="#" onclick="mostrar('wikilogo')">Change logo</a></h3>
<div id="wikilogo" style="border-collapse:collapse;display:none;">
	<form action="admin.php?do=uploadlogo" method="post" enctype="multipart/form-data">
	Logo image for the header <input type="file" name="logoimage">
    <button type="submit" class="btn">Change logo</button>
	</form>
</div>
<h3 class="admincat"><a href="#" onclick="mostrar('wikiopt')">Opciones de la wiki</a></h3>
<div id="wikiopt" style="border-collapse:collapse;display:none;">
	<form action="admin.php?do=savewikiopt" method="post">
	<table>
		<tr>
			<td>Wiki name:</td>
			<td><input type="text" name="wiki_title" value="<?php print fetchval('wiki_title'); ?>"></td>
		</tr>
		<tr>
			<td>Lema:</td>
			<td><input type="text" name="wiki_subtitle" value="<?php print fetchval('wiki_subtitle'); ?>"></td>
		</tr>
		<tr>
			<td>Default ACL (Page creation permissions):</td>
			<td>
			<select name="editpages_mintypeuser">
				<option value="1" <?php if (fetchval('editpages_mintypeuser') == 1){print "selected";}?>>Only admins can create and edit pages</option>
				<option value="2" <?php if (fetchval('editpages_mintypeuser') == 2){print "selected";}?>>Admins and privileged users can create and edit pages</option>
				<option value="3" <?php if (fetchval('editpages_mintypeuser') == 3){print "selected";}?>>All users can create and edit pages</option>
			</select> 
			</td>
		</tr>
		<tr>
			<td>Habilitar nuevos registros de usuarios:</td>
			<td>
			<select name="wiki_enablereg">
				<option value="0" <?php if (fetchval('wiki_enablereg') == 0){print "selected";}?>>Deshabilitado</option>
				<option value="1" <?php if (fetchval('wiki_enablereg') == 1){print "selected";}?>>Habilitado</option>
			</select>
			</td>
		</tr>
		<tr>
			<td>Default parser for new articles:</td>
			<td>
			<select name="defparse_type">
				<option value="0" <?php if (fetchval('defparse_type') == 0){print "selected";}?>>Wikicode (DocuWiki Standard)</option>
				<option value="1" <?php if (fetchval('defparse_type') == 1){print "selected";}?>>Markdown</option>
			</select>
			</td>
		</tr>
		<tr>
			<td>Pagina "Acerca de" (En blanco si no se quiere, el valor es la pagina, [Lo que hay despues de page= en la URL]):</td>
			<td><input type="text" name="wiki_aboutpage" value="<?php print fetchval('wiki_aboutpage'); ?>"></td>
		</tr>
	</table>
	<button type="submit" class="btn">Save changes</button>
	</form>
</div>

<h3 class="admincat"><a href="#" onclick="mostrar('wikiheader')">Header</a></h3>
<div id="wikiheader" style="border-collapse:collapse;display:none;">	
	<form action="admin.php?do=savewikiheader" method="post">
	<table>
		<tr><td colspan="2">Header external links, separate by commas (Max 4)</td></tr>
		<tr>
			<td>Names:</td>
			<td><input type="text" name="head_linktxt" value="<?php print $head_linktxt_form; ?>"></td>
		</tr>
		<tr>
			<td>URLs (¡Without http://!):</td>
			<td><input type="text" name="head_links" value="<?php print $head_links_form; ?>"></td>
		</tr>
		<tr>
			<td>Links place in header:</td>
			<td>
			<select name="headlinks_loc">
				<option value="0" <?php if (fetchval('headlinks_loc') == 0){print "selected";}?>>Left of all</option>
				<option value="1" <?php if (fetchval('headlinks_loc') == 1){print "selected";}?>>In the middle</option>
			</select> 
			</td>
		</tr>
	</table>
	<button type="submit" class="btn">Save changes</button>
	</form>
</div>

<h3 class="admincat"><a href="#" onclick="mostrar('wikiside')">Sidebar</a></h3>
<div id="wikiside" style="border-collapse:collapse;display:none;">
	<form action="admin.php?do=savewikiside" method="post">
	<table>
		<tr>
			<td>Text for sidebar (max 255 chars):</td>
			<td><textarea name="side_text"><?php print fetchval('side_text'); ?></textarea></td>
		</tr>
		<tr>
			<td>Páginas utiles de la wiki (Separar con comas, usar el tipo categoria:name, es lo que hay después de page= en la barra de direcciones)</td>
			<td><input type="text" name="side_pages" value="<?php print fetchval('side_pages'); ?>" placeholder="Ej: wiki:start"></td>
		</tr>
	</table>
	<button type="submit" class="btn">Save changes</button>
	</form>
</div>

<h3 class="admincat"><a href="#" onclick="mostrar('wikiappearance')">Appearance</a></h3>
<div id="wikiappearance" style="border-collapse:collapse;display:none;">
	<!-- include the plugin -->
	<script src="js/nativeColorPicker.js"></script>
	<form action="admin.php?do=savewikiappearance" method="post">
		<table>
			<tr>
				<td>Header color:</td>
				<td><input id="hdr_clr" name="hdr_clr" type="color" value="<?php print fetchstyle('hdr_clr'); ?>"></td>
			</tr>
			<tr>
				<td>Header separator color:</td>
				<td><input id="hdr_bar" name="hdr_bar" type="color" value="<?php print fetchstyle('hdr_bar'); ?>"></td>
			</tr>
			<tr>
				<td>Sidebar color:</td>
				<td><input id="side_clr" name="side_clr" type="color" value="<?php print fetchstyle('side_clr'); ?>"></td>
			</tr>
			<tr>
				<td>Sidebar text background color:</td>
				<td><input id="side_inclr" name="side_inclr" type="color" value="<?php print fetchstyle('side_inclr'); ?>"></td>
			</tr>
		</table>
	<a href="admin.php?do=restorewikiappearance" class="btnred">Restore default appearance</a>
	<button type="submit" class="btn">Save changes</button>
	
	</form>
	<script>
	window.nativeColorPicker.init('hdr_clr');
	window.nativeColorPicker.init('hdr_bar');
	window.nativeColorPicker.init('side_clr');
	window.nativeColorPicker.init('side_inclr');
	</script>
</div>

<?php require_once "page/footer.php"; /* Finish webpage */ ?>