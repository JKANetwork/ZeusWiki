<?php
require "functions.php";
requirelogin();
require_once "page/header.php"; //Header
$guseredit = fetchval('editpages_mintypeuser'); //General restriction to edit
$idw = idwikipage($_GET['page']);

if ($idw){ //Retrieve edit access
$mineditpage = dbw_query_fetch_array($db_conn,"SELECT edit_mintypeuser FROM pagerefs WHERE ID='$idw'"); //This checks for different edit permission
$mineditpage = $mineditpage['edit_mintypeuser']; //"Turn off" array
}else{
	$mineditpage=0; //For avoiding bug in next if	
}

//ACL checks
if ((int)$mineditpage != 0){ 
	if ((int)$mineditpage <= $user['typeuser']){die('No tienes permisos para editar la pagina');}
}else{
	if ($guseredit <= $user['typeuser']){die('No tienes permisos para editar la pagina');} 
}
/**/

if ($guseredit <= $user['typeuser']){die('No tienes permisos para editar la pagina');} //This blocks edit or create pages if mintypeuser is below the typeuser.
$timestamp = time();
if (isset($_GET['do'])){ //If it wants to do anything
	switch ($_GET['do']) {
		case "save":
			dbw_query($db_conn, "INSERT INTO pages (idpage,title,changes,editedby,text,timestamp) VALUES ('$idw','$_POST[title]','$_POST[changetext]','$user[ID]','$_POST[texteditor]','$timestamp')");
			if ($_POST['edit_mintypeuser']){ //Si se ha podido editar el "por defecto" de edicion de la pagina, editarlo
			dbw_query($db_conn, "UPDATE pagerefs SET edit_mintypeuser='$_POST[edit_mintypeuser]' WHERE ID='$idw' LIMIT 1)");
			}
			?>
				<script type='text/javascript'>
				top.location = "wiki.php?page=<?php print $idw;?>"; 
				</script>
			<?php
			break;
		case "savenew":
			$bdtitle = str_replace(" ", "_",strtolower($_POST[title])); //This generates wiki unique name for page
			
			$num = 1; //For the while (If neccesary for two equal title pages)
			$bdtitlef = $bdtitle; //Because sometimes is neccesary to change the original bdtitle
			//Now check bdtitle and cat against database,and if count != 0 add a number to end to be unique
			while (dbw_query_fetch_array($db_conn, "SELECT COUNT(ID) FROM pagerefs WHERE bdtitle = '$bdtitlef' AND idcat = '$_POST[idcat]'")[0] != 0){ 
				$bdtitlef = $bdtitle . "_" . $num;
				$num++;
			}
			//Save all
			dbw_query($db_conn, "INSERT INTO pagerefs (bdtitle,idcat,idauthor,parse_type) VALUES ('$bdtitlef','$_POST[idcat]','$_COOKIE[logued]','$_POST[parse_type]')");
			
			$idw = dbw_last_id($db_conn); //Know id of the new page (Because is saved before in pagerefs)
			dbw_query($db_conn, "INSERT INTO pages (idpage,title,changes,editedby,text,timestamp) VALUES ('$idw','$_POST[title]','$_POST[changetext]','$user[ID]','$_POST[texteditor]','$timestamp')");
			?>
				<script type='text/javascript'>
				top.location = "wiki.php?page=<?php print $idw;?>"; 
				</script>
			<?php
			break;
		case "new":
			if ($idw == "1"){$idw="";}
			?>
			<div style="width:100%;display: block;margin: 0 auto;">
			<form action="editor.php?do=savenew" method="POST">
			Categoria:
			<select name="idcat">
			<?php
				$ids=dbw_query($db_conn,"SELECT ID, catname FROM cats");
				while ($cname = dbw_fetch_row($db_conn,$ids)){
					echo '<option value="'.$cname[0].'">'.$cname[1].'</option>';
				}
			?>
			</select> <br />
			<input name="title" type="text" maxlength="70" size="65" placeholder="Titulo de la página" value=""><br />
			<textarea name="texteditor" class='texteditor'></textarea>
			Cambios:<input name="changetext" type="text" maxlength="58" size="45" required><br />
			Parser para esta página:
			<select name="parse_type">
				 <?php	$defparse_type=dbw_query_fetch_array($db_conn,"SELECT state FROM options WHERE option='defparse_type'"); ?>
					<option value="0" <?php if ($defparse_type['state'] == 0){print "selected";}?>>Wikicode</option>
					<option value="1" <?php if ($defparse_type['state'] == 1){print "selected";}?>>Markdown</option>
			</select> 
			<br><button type="submit" class="btn">Crear pagina</button>
			</form>
			</div>
			<?php
			break;
	}
}else if ($idw){ //If not,but exists the page, text editor

	?>
<div style="width:90%;display: block;margin: 0 auto;">
<?php $typepage = dbw_query_fetch_array($db_conn,"SELECT parse_type FROM pagerefs WHERE id='$idw'");
	if ($typepage['parse_type'] == 0){
		$typepage = "Wikicode";
	}else{
		$typepage = "Markdown";	
	}
	?>	
<p>Esta página esta creada usando <?php print $typepage;?></p> 
<form action="editor.php?page=<?php print $idw;?>&do=save" method="POST">
	
<?php 
	if (2 >= $user['typeuser'] && ($mineditpage == 0 || $mineditpage >= $user['typeuser'])){
		?>
			<select name="edit_mintypeuser">
				<option value="0" <?php if ($mineditpage == 0){print "selected";}?>>Seguir las opciones globales de edición para esta página</option>
				<option value="1" <?php if ($mineditpage == 1){print "selected";}?>>Solo los administradores pueden editar esta página</option>
				<option value="2" <?php if ($mineditpage == 2){print "selected";}?>>Administradores y usuarios con privilegios pueden editar esta página</option>
				<option value="3" <?php if ($mineditpage == 3){print "selected";}?>>Todos pueden pueden editar esta página</option>
			</select> 
	<?php
	}						  
									  
?>
	
Título: <input name="title" type="text" maxlength="70" size="65" value="<?php echo titlewikipage($idw)?>"><br/>
<textarea name="texteditor" class='texteditor'><?php print textwikipage($idw)?></textarea>
Cambios:<input name="changetext" type="text" maxlength="58" size="45" required><br/>
<button type="submit" class="btn">Actualizar pagina</button>
</form>
</div>
<?php
}

require_once "page/footer.php"; //Finish webpage