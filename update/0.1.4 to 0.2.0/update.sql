-- SQL update

UPDATE `system_opts` SET `value` = '0.2.0' WHERE `option` = 'wiki_version';

ALTER TABLE `users` CHANGE `sesid` `SessionID` varchar(20);