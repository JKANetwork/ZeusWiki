-- SQL update

UPDATE `system_opts` SET `value` = '0.1.3' WHERE `option` = 'wiki_version';

-- New style selectors

INSERT INTO `styles` (`style`, `value`) VALUES
('side_clr',	''),
('side_inclr',	'');

-- Maintenance mode

INSERT INTO `system_opts` (`option`, `value`) VALUES
('wiki_maintenancemode',	'0');