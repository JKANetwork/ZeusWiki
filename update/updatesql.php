<?php
/** SQL Updater for ZeusWiki */
require "../connectdata.php";
require "../parsers/dbwrapper.php"; //For database working
//Database connection
if (!isset($dbtype)){
	$dbtype="mysqli"; //Default in old versions
}
$db_conn = dbw_connect("$dbtype","$hostdb","$databasedb","$userdb","$passdb");
$version = dbw_query_fetch_array($db_conn,"SELECT value FROM system_opts WHERE option = 'wiki_version'"))['value'];

if (!$_GET['step']){//Not started
echo "<p>This will update your Wiki from $version to the lastest that you manually downloaded</p>";
echo "<p>Also, wiki will be in maintenance mode during update for preventing errors to ocurr.<p>";
echo "<p>Please make a MySQL/SQLite database backup before continue";
echo "<p>¿Are you sure to update now?</p>";
echo "<p><a href='?step=1'>Update</a></p>";
}else{
    switch ($_GET['step']){
        case "1": //Update
            if ($version != "0.1.2"){ //Turn on maintenance mode if is not 0.1.2, because 0.1.2 doesn't have it
                dbw_query($db_conn, "UPDATE `system_opts` SET `value` = '1' WHERE `option` = 'wiki_maintenancemode'");
            }
            $finished = 0;
            while ($finished == 0){
                $finished = 1; //Put this to 1, and then later if encounters and update in the folder, turn 0
                $directorio = opendir("./");
                while ($subcarpeta = readdir($directorio)) //Read all folder names searching an update
                    {
                        $check = explode(" ",$subcarpeta); //Explode for turn folder name to "version" to "fversion"
                        if ($check[0] == $version){ //Update files avaiable
                            $finished = 0; //Turn 0, one or more updates are avaiable
                            echo "<p>Updating from $version to $check[2]</p>";
							if (file_exists("$subcarpeta/update.sql")){ //Si existe un fichero común de actualización SQL
								$sqlSource = file_get_contents("$subcarpeta/update.sql");
							}else{ //Fichero difiere entre sqlite y mysql
								if ($db_conn[1] == "mysqli"){
									$sqlSource = file_get_contents("$subcarpeta/update.mysql");
								}
								if ($db_conn[1] == "sqlite"){
									$sqlSource = file_get_contents("$subcarpeta/update.sqlite");
								}
							}
                            $query = dbw_multi_query($db_conn,$sqlSource); //Update sql sentence
                            if ($query){ /* Updated */
                                //Search for update warnings file and show it
                                if (file_exists("$subcarpeta/read.txt")){echo "<p>Special instructions in this update. Read it:</p>";echo "<p style='margin-left: 2em;'>" . nl2br(file_get_contents("$subcarpeta/read.txt")) . "</p>";}
                                echo "<p>Update to $check[2] complete. Searching more...</p>";
                            }else{ /* Error detected */
                                echo "<p>Error updating from $version to $check[2]<br/>Showing SQL error:</p>";
                                print_r(mysqli_error($db_conn));
                                die("<p>You can turn off maintenance mode <a href='updatesql.php?step=2'>here</a><br/>Problems can be. You can recover from the MySQL backup I tell you to do</p>");
                            }
                            /* Updated without errors */
                            $version = $check[2]; //Update version var
                        }
                    }
                closedir($directorio);
            }
            echo "<p>Finish. Updated to $version</p>";
            echo "<p>You can turn off maintenance mode <a href='updatesql.php?step=2'>here</a></p>";
            echo "<p>If there is not showed specific instructions up to that line for updating files, continue with the readme tuto (Basically, replace all the files and folders with the downloaded ones, and remember to delete update and install folders)";
        break;
        case "2": //Turn off maintenance mode
            dbw_query($db_conn, "UPDATE `system_opts` SET `value` = '0' WHERE `option` = 'wiki_maintenancemode'");
            echo "<p>Maintenance mode turned off";
        break;
    }
}


?>