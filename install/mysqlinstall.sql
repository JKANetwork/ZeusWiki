-- Default database for wiki master
-- Adminer 4.2.5 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `cats`;
CREATE TABLE `cats` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `catname` varchar(25) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `cats` (`ID`, `catname`) VALUES
(1,	'wiki');

DROP TABLE IF EXISTS `pagerefs`;
CREATE TABLE `pagerefs` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `idcat` int(11) NOT NULL,
  `bdtitle` varchar(80) NOT NULL COMMENT 'nombre (Estilo bd)',
  `idauthor` varchar(60) NOT NULL COMMENT 'authors',
  `parse_type` tinyint(4) NOT NULL COMMENT '0-Wikicode 1-Markdown',
  `edit_mintypeuser` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `bdtitle` (`bdtitle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `pagerefs` (`ID`, `idcat`, `bdtitle`, `idauthor`, `parse_type`, `edit_mintypeuser`) VALUES
(1,	1,	'start',	'1',	0,	0),
(9,	1,	'credits',	'1',	0,	0),
(10,	1,	'help',	'1',	0,	0);

DROP TABLE IF EXISTS `pages`;
CREATE TABLE `pages` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'La id de "revision" en cierto sentido',
  `idpage` int(11) NOT NULL COMMENT 'La id de la pagina real',
  `title` varchar(100) COLLATE utf8_bin NOT NULL COMMENT 'Título',
  `changes` varchar(60) COLLATE utf8_bin NOT NULL COMMENT 'Cambios en el texto edicion',
  `editedby` int(11) NOT NULL COMMENT 'ID del editor',
  `timestamp` varchar(11) COLLATE utf8_bin NOT NULL COMMENT 'Timestamp',
  `text` text COLLATE utf8_bin NOT NULL COMMENT 'Texto del articulo',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO `pages` (`ID`, `idpage`, `title`, `changes`, `editedby`, `timestamp`, `text`) VALUES
(1,	1,	'Inicio de la wiki',	'',	1,	'',	'=====Bienvenidos a la Wiki=====\r\n**Esta es la wiki de ZeusWiki**'),
(5,	9,	'Creditos Wiki',	'Creado',	1,	'',	'===Creditos Wiki==='),
(6,	10,	'Ayuda de la wiki',	'Creado',	1,	'',	'Ayuda de la **wiki**');

DROP TABLE IF EXISTS `system_opts`;
CREATE TABLE `system_opts` (
  `option` varchar(25) COLLATE utf8_bin NOT NULL,
  `value` varchar(255) COLLATE utf8_bin NOT NULL,
  KEY `option` (`option`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO `system_opts` (`option`, `value`) VALUES
('wiki_title',	'ZeusWiki'),
('wiki_subtitle',	''),
('side_text',	''),
('side_pages',	''),
('head_links',	''),
('defparse_type',	'0'),
('editpages_mintypeuser',	'3'),
('headlinks_loc',	'0'),
('wiki_version',	'0.3.1'),
('wiki_maintenancemode',	'0'),
('wiki_aboutpage',	''),
('wiki_enablereg',	'1');

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `nick` varchar(30) NOT NULL,
  `email` varchar(40) NOT NULL,
  `passw` varchar(64) NOT NULL,
  `typeuser` varchar(64) NOT NULL DEFAULT '3' COMMENT '1-Admin, 2-Privileged users, 3-Normal users(default)',
  `SessionID` varchar(30) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `styles`;
CREATE TABLE `styles` (
  `style` varchar(25) NOT NULL,
  `value` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `styles` (`style`, `value`) VALUES
('hdr_clr',	''),
('hdr_bar',	''),
('side_clr',	''),
('side_inclr',	'');

-- 2016-01-02 18:40:00