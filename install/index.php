<DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="../page/csspc.php">
<link href='https://fonts.googleapis.com/css?family=Merriweather+Sans' rel='stylesheet' type='text/css'>
</head>
<body>
<?php

if ($_GET['step']){
	$step = $_GET['step'];
}else{
	$step == 0;
}

switch ($step) {
	case 0: //Intro form
		?>
		<p>Welcome to ZeusWiki installer</p>
		<p>In a minute, you will have your new and easy Wiki CMS installed</p>
		<p>Searching for possible problems before starting...</p>
	<?php
		touch('../connectdata.php'); //Create file
		if (! is_writable('../connectdata.php')){ //Created and writable
			echo "I can't create connectdata.php, please create it (blank file) an make it writable";
			$error=1;
		}
		if (! (function_exists('mysqli_connect') && extension_loaded('sqlite3') )) {
			echo "Neither MySQLi or sqlite3 are enabled. Please enable one of two at least before continue.";
			$error=1;
		}
		else if (function_exists('mysqli_connect') && (!extension_loaded('sqlite3'))) {
			echo "You haven't enabled SQLite3 in php, you can only use MySQLi";
		}
		else if ((!function_exists('mysqli_connect')) && extension_loaded('sqlite3')) {
			echo "You haven't enabled MySQLi in php, you can only use sqlite3";
		}
		
		if ($error == 0){ //No hay errores
		echo "<p>All right! Press next to continue.</p><br><a href='index.php?step=1' class='btn'>Next</a>";	
		}else{ //Hay errores
		echo "<p>There are errors that make install unable, please fix it.</p>";				
		}
		break;
	case 1:
		?>
		<form action="index.php?step=2" method="post">
		<p>Database connection</p>
		<table>
		<tbody>
		<tr>
			<td>
				<select name="Sel_DB" onchange="mysqli_sqlite_show()">
				<option value='mysqli'>MySQL/MariaDB</option>
				<option value='sqlite'>SQLite3</option>
				</select> 
			</td>
		</tr>
		<tr>
		<td class="textT">Server (In sqlite, location you desire, and it has to have write permission)</td>
		<td><input type="text" name="hostdb" value="localhost" required></td>
		</tr>
		<tr>
		<td class="textT">MySQL user</td>
		<td><input type="text" name="userdb" id="userdb" value="" required></td>
		</tr>
		<tr>
		<td class="textT">MySQL password</td>
		<td><input type="text" name="passdb" id="passdb" value="" required></td>
		</tr>
		<tr>
		<td class="textT">Database</td>
		<td><input type="text" name="databasedb" id="databasedb" placeholder="Wiki database" required></td>
		</tr>
		</tbody>
		</table>
		<p>Wiki data</p>
		<table>
		<tbody>
		<tr>
		<td class="textT">Wiki name</td>
		<td><input type="text" name="wiki_title" required></td>
		</tr>
		<tr>
		<td class="textT">Default Parser</td>
		<td>
			<select name="defparse_type">
				<option value="0">Wikicode (DocuWiki standard)</option>
				<option value="1">Markdown</option>
			</select>
		</td>
		</tr>
		<tr>
			<td class="textT">ACL (Permisos de creacion de paginas estandar):</td>
			<td>
			<select name="editpages_mintypeuser">
				<option value="1">Solo los administradores pueden crear y editar paginas</option>
				<option value="2" selected>Administradores y usuarios con privilegios pueden crear y editar paginas</option>
				<option value="3">Todos pueden crear y editar paginas</option>
			</select> 
			</td>
		</tr>
		</tbody>
		</table>
		<p>Admin data</p>
		<table>
		<tbody>
		<tr>
		<td class="textT">Nick</td>
		<td><input type="text" name="nick" required></td>
		</tr>
		<tr>
		<td class="textT">E-Mail</td>
		<td><input name="email" type="text" required></td>
		</tr>
		<tr>
		<td class="textT">Password</td>
		<td><input name="pass" type="password" required></td>
		</tr>
		<tr>
		<td class="textT">Repeat password</td>
		<td><input name="passver" type="password" required></td>
		</tr>
		</tbody>
		</table>
		<p>&nbsp;</p>
		<p>This and other options will be in admin panel of wiki</p>
		<table>
			<tr>
			<td><button type="submit" class="btn">Install</button></td>
			</tr>
		</table>
		</form>
		<?php
		break;
	case 2:
		//Comprobacion del usuario
		$nick=$_POST['nick'];
		$passw=hash("sha256",$_POST['pass']);
		$email=$_POST['email'];
		if ($_POST['pass'] != $_POST['passver']){
			die("<h2>Password is miswritted...</h2>");
		}
		
		if ($_POST[Sel_DB] == "mysqli"){
			//Database connect
			$installconnect = mysqli_connect($_POST['hostdb'],$_POST['userdb'],$_POST['passdb'],$_POST['databasedb']) //host,user,pass,basededatos
			or die ("No se puede conectar con mysql, revise los datos escritos");
			echo "Installing database...<br/>";
			$sqlSource = file_get_contents('mysqlinstall.sql');
			mysqli_multi_query($installconnect,$sqlSource);

			echo "Writting connect options to file<br>";
			//Escribir conexion
			$file = fopen("../connectdata.php", "w");
			fwrite($file, "<?php" . PHP_EOL);
			fwrite($file, "//Data for connection" . PHP_EOL);
			fwrite($file, '$dbtype="mysqli";' . PHP_EOL);
			fwrite($file, '$hostdb="'.$_POST[hostdb].'"; //Host' . PHP_EOL);
			fwrite($file, '$userdb="'.$_POST[userdb].'"; //Database user' . PHP_EOL);
			fwrite($file, '$passdb="'.$_POST[passdb].'"; //Database password' . PHP_EOL);
			fwrite($file, '$databasedb="'.$_POST[databasedb].'"; //Database of database server' . PHP_EOL);
			fclose($file);

			echo "Configuring wiki<br>";
			//Insertar usuario
			mysqli_query($installconnect,"INSERT INTO `users`(`nick`, `passw`, `email`) VALUES ('$nick','$passw','$email')");	
			mysqli_query($installconnect, "UPDATE system_opts SET value='$_POST[wiki_title]' WHERE option='wiki_title'");
		
		}else if ($_POST['Sel_DB'] == "sqlite"){ //Sqlite install
			echo "Installing database...<br>";
			copy('sqlitedb.sqlite3', '../'.$_POST[databasedb]); //Copy DB to new location

			echo "Writting connect options to file<br />";
			//Escribir conexion
			$file = fopen("../connectdata.php", "w");
			fwrite($file, "<?php" . PHP_EOL);
			fwrite($file, "//Data for connection" . PHP_EOL);
			fwrite($file, '$dbtype="sqlite";' . PHP_EOL);
			fwrite($file, '$hostdb="'.$_POST[hostdb].'"; //Host' . PHP_EOL);
			fwrite($file, '$userdb="'.$_POST[userdb].'"; //Database user' . PHP_EOL);
			fwrite($file, '$passdb="'.$_POST[passdb].'"; //Database password' . PHP_EOL);
			fwrite($file, '$databasedb="'.$_POST[databasedb].'"; //Database of database server' . PHP_EOL);
			fclose($file);
			
			echo "Configuring wiki<br />";
			$db_conn = new SQLite3($_POST[databasedb]); //Connect
			//Insertar usuario
			$nick=$_POST['nick'];
			$db_conn->query("INSERT INTO `users`(`nick`, `passw`, `email`) VALUES ('$nick','$passw','$email')");	
			$db_conn->query("UPDATE system_opts SET value='$_POST[wiki_title]' WHERE option='wiki_title'");
		}
	echo "Instalado, ve a la página principal para comenzar";
	break;
}
?>
	
<script>
function mysqli_sqlite_show(){
	var sel = document.getElementsByName("Sel_DB")[0].value;
	switch(sel) {
		case 'mysqli':
			document.getElementById("databasedb").removeAttribute("disabled");
			document.getElementById("userdb").removeAttribute("disabled");
			document.getElementById("passdb").removeAttribute("disabled");
			document.getElementById("databasedb").value='';
			document.getElementById("userdb").value='';
			document.getElementById("passdb").value='';
		break;
			
		case 'sqlite':
			document.getElementById("databasedb").disabled='true';
			document.getElementById("userdb").disabled='true';
			document.getElementById("passdb").disabled='true';
			document.getElementById("databasedb").value='NOT NEEDED';
			document.getElementById("userdb").value='NOT NEEDED';
			document.getElementById("passdb").value='NOT NEEDED';
		break;
			
	}
}
mysqli_sqlite_show();
</script>
	
	