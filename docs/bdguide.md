# MySQL bd guide .
Tables:
cats: Categories
pagerefs: The general data about the pages (Real ID of pages, author and edit permission)
pages: All the "text" of the pages, revisions, etc (pagerefs "links" to this for load pages)
system_opts: Options of ZeusWiki
users: Users of the wiki
styles: Custom styles page

---
**system_opts**
system\_opts is based in "option" and "value" in the table

option:
wiki_title: Easy to know
wiki_subtitle: Easy to know
side_text: Text of side
side_pages: Links in left part
head_links: External links in header
defparse_type: Standard parse type of new pages
editpages_mintypeuser: Minimal position of user for edit pages ( 1-Admin, 2-Privileged users, 3-Normal users(default) )
headlinks\_loc: Location of head_links
wiki_aboutpage: About page (Link to it, in admin opts)
wiki_version: Version of ZeusWiki

---
**pagerefs**
ID: Real ID of a wiki page
idcat: Category of the page (Is equal to a cats.ID)
bdtitle: Title (for URL) of the page (Example: category:title)
idauthor: ID of the author
parse_type: Changes default parser for a parser only of this page
edit\_mintypeuser: Minimal position of user for edit pages ( 0-Like system_opts, 1-Admin, 2-Privileged users, 3-Normal users(default) )

---
**pages**