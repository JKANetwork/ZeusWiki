# ZeusWiki, a simple and light wiki

ZeusWiki is a simple yet powerful wiki cms witch aims to have a simple wiki page for documentation, community tutorial, and similar things, 
with a certain grade of personality style and links, fast and very light.

## Disclaimer

This is an beta, opensource, and human made wiki cms, **there may be bugs and problems**, and
although we can help or fix things, JKANetwork is **not** responsable for problems.

**All the documentation is (and will be) in docs folder**

You can "surf" the master code and tell us any improvment or bug. Plase see indications for installing if you want to install instead of downloading master and install, it will not work right.

## Requirements

-Modern PHP (5.5+)

-For MySQLi: MySQL/MariaDB installed with a database for the wiki and php MYSQLi extension

-For SQLite3: SQLite3 php extension enabled and a location (With write permisson) for the DB

## Install

For installing, only download last version from https://gitlab.com/JKANetwork/ZeusWiki/tags (not git master, reason below. **Think of tags as releases**)
, decompress it and copy all to the folder in your hosting. 

Please **do not download and use master git**,it is not recommended, because is for devel and things like installer sql can not work right. Use the tags. **Think of tags as releases**

Go to (example.org is example, replace with your place): example.org/install/

When installed, please delete install folder

Test it and tell me bugs or show me your page ;)

## Update

**Please read everything before start**

In the **update** folder there are subfolders (starting with ver 0.1.2 to 0.1.3) for updating

Updates has to be done in two steps. SQL Update and files update

First, download the lastest release from https://gitlab.com/JKANetwork/ZeusWiki/tags (Like in install, lastest tag, not master)

Decompress the zip in your computer (like for a install), and you will see a update folder, and inside a lot of subfolders. 
**We will centrate for now in update folder, but not delete the rest, you will have to use it later**

The folders are named (X.Y.Z to A.B.C) and in it there is an update.sql file (And in some, read.txt files that will be showed later).

You have to **copy update folder** to the wiki (For save space, you only have to copy from your version to lastest version folders, before will not be used.)

_Example: You are in version 0.1.3 and you will update to 0.1.4_

_There are "0.1.2 to 0.1.3" and "0.1.3 to 0.1.4" folders. In web update folder, only is neccesary "0.1.3 to 0.1.4" subfolder_

**Also**, you have to **copy the updatesql.php** file of update folder, do not delete it

Open updatesql.php in browser (example.org/update/updatesql.php)

updatesql.php will turn on maintenance mode of the wiki, and will update sql database.
Then, it will show you if errors ocurred and say you how to proceed with file updates

Also, it will take you a link to turn off maintenance mode when you finish (Plase **don't lose it**!)

If updatesql.php does not say anything about how to proceed with file updates, you have to replace the rest of files (All **minus update, install, and images**) with the downloaded ones

Turn off maintenance mode (With link that updatesql.php took you), delete update folder of wiki, and you are finish.

We try to not lose anything with updates, and not make bugs appear, but, its not 100% bulletproof, **make a copy of MySQL database before updating** (And if you want, of folder)

## Changelog

### Version 0.3.1

-Fix some bugs like registering users

-A tiny bit improved updater

### Version 0.3

-Comply with HTML5 w3c validator

-Implement dbwrapper (for SQLite3)

-SQLite3 support (Experimental)

-Some optimizations 

-Bugfix in installer

### Version 0.2

-Wiki is functional as is

-Update markdown parser (parsedown)

-Restructure some head code

-Rework login system

-Fix register bugs

### Version 0.1.4

-Better style colors and sizes for texts

-Option to disable register

-Translation to English of wiki, preparing for the multilang (English/Spanish at first) option

-Fixed bugs in oldversions page

-Fixes for installing and updating

### Version 0.1.3

-More color selecting (sidebar)

-Fix style selector bug

-First version with update system and primitive maintenance mode for it

-Translation of some things to the international English (In the future, will be Spanish and English together)

### Version 0.1.2

-Change mode of selecting custom colors (New SQL table styles that is not in V0.1.1)

-New about page option, now works (Select it in admin,there is empty by default)

-New form to generare unique wiki URL without prompts

-Version now stored in sql (system_opts.wiki\_version, there is not in V0.1.1)

-Fix some minor bugs

If you "update" from V0.1.1 you have to do manually changes in your MySQL tables:

.Add new table styles (There is in bottom of sqlinstall.sql)

.Add in system\_opts two new items: "wiki\_version (with 0.1.2 in it)" and "wiki_aboutpage (empty)"

### Version 0.1.1

-Solve permission problems for edit pages and create edit permissions for every page

-Solve bugs editing in admin panel

### Version 0.1

-Re construct code, not compatible with past versions. Change some documentation in docs folder