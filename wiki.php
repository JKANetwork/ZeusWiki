<?php
require "functions.php";
require_once "page/header.php"; //Header
$wiki_title = fetchval('wiki_title');
if (! $_GET['page']){
	$page="wiki:start";
}else{
	$page=$_GET['page'];
}
$id=idwikipage($page); //Function that returns id of page, or name of a category. (Or none)
if (is_numeric($id)){ //If its a number, is the ID of the page
	?>
	<h1><?php print titlewikipage($id); ?></h1>
	<script>document.title = "<?php print titlewikipage($id) ?> - <?php print $wiki_title['text'] ?>";</script>
	<div id="TOCdiv"></div>
	<script src="js/TOC.js"></script>
	<?php
	parsewikipage($id); //Parses (writes) the page
	$pagethings = dbw_query_fetch_array($db_conn, "SELECT ID,changes,editedby,timestamp FROM pages WHERE idpage='$id' ORDER BY ID DESC LIMIT 1");
	$editbyname = dbw_query_fetch_array($db_conn, "SELECT ID,nick FROM users WHERE ID='$pagethings[editedby]'");
	echo "<br><p>Ultima edicion ";
	if ((int)$pagethings['timestamp'] != 0){
	echo "el " . date("d-m-Y", (int)$pagethings['timestamp']) . " (UTC) ";
	}
	echo "por ".$editbyname['nick'].". Razon: " .$pagethings['changes']. "</p>";
	echo "<p><a href='oldversions.php?page=".$_GET['page']."'>Ver versiones anteriores</a></p>";
	}else if (substr($id, 0,1) == "C"){ //Category
		$id  = substr($id, 1);
		$catname = dbw_query_fetch_array($db_conn, "SELECT * FROM cats WHERE ID='$id' ");
		echo "Categoría: $catname[1]<br>";
		$ids = dbw_query($db_conn, "SELECT ID,bdtitle FROM pagerefs WHERE idcat ='$id'");
		while ($wname = dbw_fetch_array($db_conn,$ids)){
			$didwhile=1;
			echo "<a href='wiki.php?page=$catname[1]:$wname[1]'>".titlewikipage($wname[0])."</a>"; //wname 0 is ID, wname 1 is name of pageref for the page
			echo "<br>";
		}
	if ($didwhile != 1){
		echo "<p>La categoría está vacia</p>";
		if ( 1 == $user['typeuser']){
			?>
			<form name="borrarcat" action="admin.php?do=borrarcat&cat=<?php print $catname[1] ?>" method="post" >
			<p><a href="#" onClick="pregunta()">Delete category</a></p>
			</form>
			<script language="JavaScript">
			function pregunta(){
				if (confirm('Are you sure of deleting this category? You cant go back later')){
				   document.borrarcat.submit()
				}
			}
			</script>

		<?php
		}
	}

	}else{ //No post
	?>
	<h1>There is nothing here (yet)<h1>
	<?php
}

require_once "page/footer.php"; //Finish webpage
