<?php
require "functions.php";
require_once "page/header.php"; //Header
if (! $_GET['page']){
	$page="wiki:start";
}else{
	$page=$_GET['page'];
}

$id=idwikipage($page); 

if ($_GET['do'] == "delete" && $user['typeuser'] == 1){ //If it wants to delete lastest rev and is an admin
	dbw_query($db_conn, "DELETE FROM pages WHERE idpage='$id' ORDER BY ID DESC LIMIT 1"); //This erases lastest rev of a page
}
if (is_numeric($id)){ //If its a number, the ID of the page
	$titlepage=titlewikipage($id);
	?>
	<h1><?php print $titlepage; ?> - Changes in this page</h1>
	<script>document.title = "<?php print $titlepage ?> - <?php print fetchval('wiki_title'); ?>";</script>
	<?php
	
	$pagethings = dbw_query($db_conn, "SELECT ID,changes,editedby,timestamp FROM pages WHERE idpage='$id' ORDER BY ID DESC");
	//wpage in while will be every revision/change of the page
	while ($wpage = dbw_fetch_array($db_conn,$pagethings)){
			$howmuch++; //How much revisions of the page are
			$editbyname = dbw_query_fetch_array($db_conn, "SELECT ID,nick FROM users WHERE ID='$wpage[editedby]'");
			echo "<a href='wiki.php?page=$page&revid=".$wpage['ID']."'>".$titlepage." - Edited by: ".$editbyname['nick'];
			if ((int)$wpage['timestamp'] != 0 ){
				echo " at ".date("d-m-Y", (int)$wpage['timestamp']) . " (UTC)";
			}
			echo ". Reason: ".$wpage['changes']."</a>";
			echo "<br/>";
	}
	echo "<br>";
	//If more than 1 rev, option to delete lastest revision of the page
	if ($_COOKIE['logued'] && 2 >= $user['typeuser'] && $howmuch > 1){ 
		?>
		<button class="btnred" onClick="eraserev()">Erase last change of the page</button>
			<script language="JavaScript">
				function eraserev(){
					if (confirm('¿Estas seguro de borrar la ultima revisión y volver a la anterior?')){
					   top.location = "oldversions.php?do=delete&page=<?php print $page;?>";
					}
				}
			</script>
		<?php
	}
	}else{ //Not valid (Category or null)
	?>
	<h1>Not valid page<h1>
	<?php
}
require_once "page/footer.php"; //Finish webpage