<?php header("Content-type: text/css"); 
require "../connectdata.php";
require_once "../functions.php"; //For fetchstyle and connect function

?>
body {
	margin:0px;	
	margin-left:1.5em;
	margin-right:1.5em;
	font-family: 'Merriweather Sans', sans-serif;
	min-width:1024px;
}

table.headtable {
	padding:0.5em;
	margin-left:-1.5em;
	margin-right:-1.5em;
	background-color:<?php print fetchstyle('hdr_clr');?>;
	margin-bottom:0.5em;
}
hr {
	border: 5px solid <?php print fetchstyle('hdr_bar'); ?>;
	margin-top: -1em;
	margin-left:-1.5em;
	margin-right:-1.5em;
}
div.menu {
	text-align: right;
	font-size: 15px;
	font-weight: bold;
	font-family: sans-serif;
	vertical-align: left;
	padding-left:1em;
}
div.menu a {
    color: black;
	text-decoration: underline overline;
}
div.menu nav {
	margin: 0px auto;
	text-align: center;
	display:inline-block;
}
div.menu li {
	display: inline-block;
	padding: 1em;
}

.admincat {
	/*color: black;*/
	background-color: #ccc;
	padding: 0.3em;
	border-radius: 8px;
}

.admincat a{
	color:black;
	text-decoration:underline;
}

textarea.texteditor {
	width: 100%;
	height: 400px;
}

a {
	text-decoration:none;
	color:#1570d7;
}

#TOCdiv {
	background-color: #eff1ff;
	display: table;
	padding: 0.5em;
	border: 1px solid #ccc;
	font-size:14px;
}

#leftside {
	width:180px;
	margin-right:20px;
	float:left;
	font-size:10px;
	padding:5px;
	padding-bottom:50px;
	background-color:<?php print fetchstyle('side_clr');?>;
}

div.fondolat {
	background-color:<?php print fetchstyle('side_inclr');?>;
	border:1px solid grey;
}

/** Login logout buttons (Of side) */
div.tologbtn {
	float: left;
	font-size: 14px;
	font-family: sans-serif;
	background-color: <?php print fetchstyle('side_clr');?>;
	padding: 5px;
}

div.content {
	display: table-cell;
	min-width:750px;
}

code {
	display: block;
	background-color: #e8e8e8;
	border: 1px solid #a0a0a0;
	padding:0.5em;
	margin: 1.5em 0em 1.5em;
}

a,p,h1,h2,h3,h4,h5,h6{
	font-family: sans-serif;
}

p,code{
	font-size:12px;
}

h1 { font-size: 24px;}
h2 { font-size: 22px;}
h3 { font-size: 18px;}
h4 { font-size: 16px;}
h5 { font-size: 12px;}
h6 { font-size: 10px;}


/* Formularios bonitos */
.textT {
text-align:right;font-size:13px;font-weight: bold;color:#222
}
.btn {
color: #fff;
background-color: #428bca;
display: inline-block;
text-align: center;
margin-top: 5px;
cursor: pointer;
background-image: none;
border: 1px solid transparent;
padding: 6px 12px;
font-size: 14px;
line-height: 1.42857143;
border-radius: 4px;
}
.btnred {
color: #fff;
background-color: #D30B0B;
display: inline-block;
text-align: center;
margin-top: 5px;
cursor: pointer;
background-image: none;
border: 1px solid transparent;
padding: 6px 12px;
font-size: 14px;
line-height: 1.42857143;
border-radius: 4px;
}
input[type=text],input[type=password], textarea {
  transition: all 0.30s ease-in-out;
  outline: none;
  padding: 3px 0px 3px 3px;
  margin: 5px 1px 3px 0px;
  border: 1px solid #DDDDDD;
  display: block;
  width: 100%;
  height: 34px;
  font-size: 14px;
  color: #555;
  background-color: #fff;
  border: 1px solid #ccc;
  border-radius: 4px;
}
 
input[type=text]:focus,input[type=password]:focus, textarea:focus {
  box-shadow: 0 0 5px rgba(81, 203, 238, 1);
  padding: 3px 0px 3px 3px;
  margin: 5px 1px 3px 0px;
  border: 1px solid rgba(81, 203, 238, 1);
}

/* Fin formularios bonitos */

