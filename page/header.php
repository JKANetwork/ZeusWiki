<!DOCTYPE html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" href="page/csspc.php">
<link href='https://fonts.googleapis.com/css?family=Merriweather+Sans' rel='stylesheet' type='text/css'>
<title><?php print fetchval('wiki_title'); ?></title>
</head>
<body>
<?php
$headlinks_loc = fetchval('headlinks_loc');
function headlinks() {
	global $user;
	global $conexion;
	$E_head_links = explode(",",fetchval('head_links')); //Exploded by commas head_links
	$countt = count($E_head_links);
	if (fetchval('head_links')){
		for ($i = 0; $i <= $countt; $i = $i + 2){ //Show links if it exists
			if ($E_head_links[$i]){
				$m = $i + 1;
				echo "<li><a href='http://$E_head_links[$i]'>$E_head_links[$m]</a></li>";
			}
		}
	}
}
?>
<table class="headtable">
<tr>
<td style="width:310px;">
	<a href="index.php"><img src="images/logoheader.png" alt="Logo" style="width:200px;"></a>
</td>
<td style="width:100%">
	<div class="menu">
	<nav>
	<ul>
	<?php if ($headlinks_loc== 0){headlinks();} ?>
	<li><a href='index.php'>Home</a></li>
	<li><a href='pagesindex.php'>Index</a></li>
	<?php	
	if ($headlinks_loc == 1){headlinks();}
	global $user;
	if ( 1 == $user['typeuser']){echo "<li><a href='admin.php'>Admin</a></li>";}
	$aboutpage = fetchval('wiki_aboutpage');//"Cache" aboutpage value
	if ($aboutpage != ""){
		echo "<li><a href='wiki.php?page=$aboutpage'>About</a></li>";
	}
	?>
	</ul>
	</nav>
	</div>
</td>
</tr>
</table>
<hr>
<?php
if ($_COOKIE['SessionID']){ //Logued
	echo '<div class="tologbtn"><a style="color:#328cf2;" href="logout.php">Logout</a></div>';
	if (basename($_SERVER['SCRIPT_NAME']) == "wiki.php"){
		if (! $_GET['page']){$_GET['page'] = "1";}
		echo '<div style="float:right;margin-right:1.5em;font-size:14px;font-family:sans-serif;"><a style="color:#328cf2;" href="editor.php?do=new">New page</a>&nbsp;&nbsp;<a style="color:#328cf2;" href="editor.php?page='.$_GET[page].'">Edit this page</a></div>';
	}
}else{ //Not logued
	echo '<div class="tologbtn"><a style="color:#328cf2;" href="login.php">Login</a>';
	//Show register only if it can register
	if (fetchval('wiki_enablereg') == 1){echo '&nbsp;&nbsp;<a style="color:#328cf2" href="register.php">Register</a>';}
	echo '</div>';
}
echo '<div style="clear:both;"></div>';
	
require_once "page/side.php"; //With this, left side is activated.
	
echo '<div class="content" id="content">'; //Div of content