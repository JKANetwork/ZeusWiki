<?php
require "connectdata.php";
require "parsers/dbwrapper.php"; //For database working
//Database connection
$db_conn = dbw_connect("$dbtype","$hostdb","$databasedb","$userdb","$passdb");

/** See if its logued correctly */
if (isset($_COOKIE["SessionID"]))
{
	$user = dbw_query_fetch_array($db_conn, "SELECT * FROM users WHERE SessionID = '$_COOKIE[SessionID]'");
	if ($user['SessionID'] != $_COOKIE['SessionID'])
	{
		setcookie("SessionID", "", time()-3600);
		echo "ERROR";
		?>
			<script type='text/javascript'>
			top.location = "index.php"; 
			</script>
		<?php
		die();
	}
}	

/** Returns value for and option of the website */
function fetchval($str){
	global $db_conn;
	$value = dbw_query_fetch_array($db_conn,"SELECT value FROM system_opts WHERE option = '$str'")['value'];
	return $value;
}

/** Primitive "take down all because we are in maintenance mode" */
if (fetchval("wiki_maintenancemode") == 1){
	die("<p>We are in maintenance, plase come back later</p>");
}


/** Function to return custom style */
function fetchstyle($str){
	global $db_conn;
	$customstl = dbw_query_fetch_array($db_conn,"SELECT value FROM styles WHERE style = '$str'");
	if (! $customstl['value']){ //If not custom style applied
		switch ($str){ //Case select default style
			case "hdr_clr":
				return "#848484";
			break;
			case "hdr_bar":
				return "#3c7ef5";
			break;
			case "side_clr":
				return "#c6c6c6";
			break;
			case "side_inclr":
				return "#DDD";
			break;
		}
	}else{ //If custom style applied
		return $customstl['value'];
	}
}

function idwikipage($str) {
	global $db_conn;
	if (! $str){$str = "1";}
	// First, we have to see if it is a number, or a string
	if (is_numeric($str)){ //Its a number
		$idpage = dbw_query_fetch_array($db_conn, "SELECT ID FROM pagerefs WHERE ID='$str'")[0];
	}else{ //Is a string
		if (strpos($str, ":")) { //Its a name of a page with cat	
			$str = explode(":", $str);
			$idcat = dbw_query_fetch_array($db_conn, "SELECT ID FROM cats WHERE catname ='$str[0]' LIMIT 1");
			$idpage = dbw_query_fetch_array($db_conn, "SELECT ID FROM pagerefs WHERE bdtitle='$str[1]' AND idcat ='$idcat[0]' LIMIT 1");
			$idpage = $idpage[0];
			if (! $idpage){$idpage = "N";}
			}else{ //Only the category
			$idpage = dbw_query_fetch_array($db_conn, "SELECT * FROM cats WHERE catname LIKE '%$str%'");
			$idpage = "C" . $idpage[0];
		}
	}
	return $idpage;
}	

function parsewikipage($idpage) {
	global $db_conn;
	$parsetype = dbw_query_fetch_array($db_conn, "SELECT ID,parse_type FROM pagerefs WHERE ID='$idpage'"); //Knowing what parser is used for this page
	if ( $_GET['revid']){ //If I have to use other revision of the page
	$textr = dbw_query_fetch_array($db_conn, "SELECT ID,text FROM pages WHERE idpage='$idpage' AND ID='$_GET[revid]' ORDER BY ID DESC LIMIT 1");
	}else{
		$textr = dbw_query_fetch_array($db_conn, "SELECT ID,text FROM pages WHERE idpage='$idpage' ORDER BY ID DESC LIMIT 1");
	}
	switch ($parsetype['parse_type']){
		case 0: //If parser is Wikicode
			require_once "parsers/wikirenderer/WikiRenderer.lib.php";
			$ctr=new WikiRenderer("dokuwiki_to_xhtml");
			$texte=$ctr->render($textr['text']);
			echo $texte;
		break;
		case 1: //If parser is Markdown
			require_once "parsers/parsedown.php";
			echo Parsedown::instance()
				->setMarkupEscaped(true) # escapes markup (HTML)
				->text($textr['text']);
		break;
	}
}

function textwikipage($idpage) {
	global $db_conn;
	$textr = dbw_query_fetch_array($db_conn, "SELECT text FROM pages WHERE idpage='$idpage' ORDER BY ID DESC LIMIT 1");
	return $textr[0];
}
function titlewikipage($idpage) {
	global $db_conn;
	$textr = dbw_query_fetch_array($db_conn, "SELECT title FROM pages WHERE idpage='$idpage' ORDER BY ID DESC LIMIT 1");
	return $textr[0];
}

/** Simple function to die if it's not logued */
function requirelogin(){
	if (! $_COOKIE['SessionID']){
		die("<p>You have to login first</p><p><a href='index.php'>Return to index</a></p>");
	}
}

function RandomString($length)
{
	//https://phpes.wordpress.com/2007/06/12/generador-de-una-cadena-aleatoria/
    $source = 'abcdefghijklmnopqrstuvwxyz';
    $source .= '1234567890';
    if($length>0){
        $rstr = "";
        $source = str_split($source,1);
        for($i=1; $i<=$length; $i++){
            mt_srand((double)microtime() * 1000000);
            $num = mt_rand(1,count($source));
            $rstr .= $source[$num-1];
        }

    }
    return $rstr;
}