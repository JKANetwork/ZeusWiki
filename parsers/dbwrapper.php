<?php
/*	DBWrapper.php - Version 1.4
	This script is a simple wrapper for SQLite3, MySQL and PgSQL, 
	for make possible to use different BD systems without changing the functions.
	For use, in dbw_connect you have to specify type of database (see below)
	Avaiable functions:
	dbw_connect ($tdb,$server,$database,$user,$password) -> some values are optional, 
	except $tdb and $server. $server is location in SQLite3
	dbw_close ($conn) -> Closes connection

	dbw_query ($conn,$query) -> Does a query
	dbw_multi_query($conn,$multiquery) -> This does a multiquery without returning nothing. 
										  Its used for load from a file/script
	dbw_query_fetch_array ($conn,$query[,$typearray]) -> This do a query and fetch array, all in one function 
														 ($typearray optional, see below)

	--After here, this functions works with result of a query--

	dbw_fetch_array ($conn,$result[,$typearray]) -> Fetch a row. ($typearray optional, see below)
	dbw_escape_string($conn,$string) -> Escapes conflictive chars for inserting into database

	dbw_fetch_row and dbw_fetch_assoc ($conn,$result) -> Wrappers of dbw_fetch_array with row or assoc arguments
	dbw_query_goto($conn,$result[,$row]) -> Goto X result of a query. If row is not specified, will be first row, 0
	dbw_num_rows($conn,$result) -> Return number of results of a query

    --This doesnt need a query--
	dbw_last_id($conn) -> Returns last insert ID
	dbw_insert_id($conn) -> Alias of dbw_last_id

	
	$tdb (Type of database) can be:
		-mysql/mysqli -> MySQL or MariaDB
		-sqlite/sqlite3 -> Sqlite3
		-PostgreSQL/PgSQL/pg -> PostgreSQL
		
	$conn is the connection stablished in dbw_connect (ie. $conn = dbw_connect('sqlite','file.sqlite'))
	$typearray is the form of array is returned, and not writed is default:
		-ASSOC -> Associative indexes
		-NUM -> Numeric indexes
		-BOTH -> (Default) Both types of indexes
*/


/** Connect with database */
function dbw_connect($tdb,$server,$database=NULL,$user = NULL,$password=NULL){
	switch ($tdb){
		case "mysql":
		case "mysqli":
		$return[0] = mysqli_connect($server,$user,$password,$database) or die("Error de conexion");
		$return[1] = "mysqli"; //Return standard mysqli for other funcs.
		break;
		case "sqlite":
		case "sqlite3":
		$return[0] = new SQLite3($server);
		if (!$return[0]) die ("Error de conexion");
		$return[1] = "sqlite"; //Return standard SQLite3 for other funcs.
		break;
		case "PostgreSQL":
		case "pg":
		case "PgSQL":
		$return[0] = pg_connect("host=$server dbname=$database user=$user password=$password") or die ('Error de conexion: ' . pg_last_error());
		$return[1] = "PgSQL"; //Return standard PgSQL for other funcs.
		break;
		default:
			return false;
		break;
	}
	return $return;
}

/** Escapes conflictive chars for inserting into database */
function dbw_escape_string($conn,$string){
    switch ($conn[1]){
        case "mysqli":
            return mysqli_escape_string($conn[0],$string);
        case "sqlite":
            return SQLite3::escapeString($string);
        case "PgSQL":
            return pg_escape_string($string);
    }
}


/** Make query */
function dbw_query($conn,$query){
	switch ($conn[1]){
		case "mysqli":
			return mysqli_query($conn[0],$query);
		break;
		case "sqlite":
			return $conn[0]->query($query);
		break;
		case "PgSQL":
			return pg_query($query); //Last error (pg_last_error()) not implemented
		break;
		default:
			return false;
		break;
	}
}

/** Fetch array from query */
function dbw_fetch_array($conn,$result,$typearray = NULL){
	if ($result == false || $result == NULL){return false;}
	switch ($conn[1]){
		case "mysqli":
			if ($typearray == NULL || $typearray == "BOTH"){return mysqli_fetch_array($result);}
			if ($typearray == "ASSOC"){return mysqli_fetch_array($result,MYSQLI_ASSOC);}
			if ($typearray == "NUM"){return mysqli_fetch_array($result,MYSQLI_NUM);}
		break;
		case "sqlite":
			if ($typearray == NULL || $typearray == "BOTH"){return $result->fetchArray();}
			if ($typearray == "ASSOC"){return $result->fetchArray(SQLITE3_ASSOC);}
			if ($typearray == "NUM"){return $result->fetchArray(SQLITE3_NUM);}
		break;
		case "PgSQL":
			if ($typearray == NULL || $typearray == "BOTH"){return pg_fetch_array($result);}
			if ($typearray == "ASSOC"){return pg_fetch_array($result,NULL,PGSQL_ASSOC);}
			if ($typearray == "NUM"){return pg_fetch_array($result,NULL,PGSQL_NUM);}
		break;
		default:
			return false;
		break;
	}
}

/** Make query and fetch array */
function dbw_query_fetch_array($conn,$query){
	switch ($conn[1]){
		case "mysqli":
			$query = mysqli_query($conn[0],$query);
			if ($query == false || $query == NULL){return false;}
			return mysqli_fetch_array($query);
		break;
		case "sqlite":
			$query = $conn[0]->query($query);
			if ($query == false || $query == NULL){return false;}
			return $query->fetchArray();
		break;
		case "PgSQL":
			$query = pg_query($query);
			if ($query == false || $query == NULL){return false;}
			return pg_fetch_array($query); //Last error (pg_last_error()) not implemented
		break;
	}
}

/** Goes a query to $row. $row starts in 0 as first row as if not specified */
function dbw_query_goto($conn,$result,$row = 0){
	switch ($conn[1]){
		case "mysqli":
			mysqli_data_seek($result,$row);
		break;
		case "sqlite":
			$result->reset();
			$count = 0;
			while ($count != $row){
				$result->fetchArray();
			}
		break;
		case "PgSQL":
			pg_result_seek($result, $row);
		break;
	}
}

/** Does multiple querys in one command */
function dbw_multi_query($conn,$query){
	switch ($conn[1]){
		case "mysqli":
			mysqli_multi_query($conn[0],$query);
		break;
		case "sqlite":
			$conn[0]->exec($query);
		break;
		case "PgSQL":
			die ("No soportado aun"); // 		TODO
		break;
	}
}

/** Returns the lastest Insert ID */
function dbw_last_id($conn){
	switch ($conn[1]){
		case "mysqli":
			return mysqli_insert_id($conn[0]);
		break;
		case "sqlite":
			return $conn[0]->lastInsertRowID();
		break;
		case "PgSQL":
			return pg_fetch_array(pg_query("SELECT lastval();"))[0];
		break;
	}
}


/** Returns number of results */
function dbw_num_rows($conn,$result){
	switch ($conn[1]){
		case "mysqli":
			return mysqli_num_rows($result);
		break;
		case "sqlite":
			die ("No soportado aun"); // 		TODO
		break;
		case "PgSQL":
			die ("No soportado aun"); // 		TODO
		break;
	}
}


/** Close connection */
function dbw_close($conn){
	switch ($conn[1]){
		case "mysqli":
			mysqli_close($conn[0]);
		break;
		case "sqlite":
			$conn[0]->close();
		break;
		case "PgSQL":
			pg_close($conn[0]);
		break;
	}
}

/** Some internal wrappers for functions that are equal to other with arguments */

function dbw_fetch_assoc($conn,$result){return dbw_fetch_array($conn,$result,"ASSOC");}
function dbw_fetch_row($conn,$result){return dbw_fetch_array($conn,$result,"NUM");}
function dbw_insert_id($conn){return dbw_last_id($conn);}

?>